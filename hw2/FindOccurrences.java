import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class FindOccurrences {
	
	public static void main(String[] args) {
		int [] matrix = readMatrix();
		
		for (int i = 0; i < matrix.length; i++) {
			System.out.println(i + " occurs " + matrix[i] + " time(s)");
		}
	}


		
	private static int[] readMatrix(){
		int[][] matrix = new int [10][10];
		File file = new File("C:\\Users\\ozank\\eclipse-workspace\\demo\\src\\matrix.txt");
		int[] count = new int[10];
		
	    try {

	        Scanner sc = new Scanner(file);
	        int i = 0;
	        int j = 0;			
	        while (sc.hasNextLine()) {
	            int number = sc.nextInt();
	            matrix[i][j] = number;
	            if (j == 9)
	            	i++;
	            j = (j + 1) % 10;
	            
	            
				for (int k = 0; k < count.length; k++) {
					if (k == number) {
						count[k] += 1;
					}
				}	                
	            if (i==10)
	            	break;
	        }
	        sc.close();
	    } 
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }		
		return count;
	}
	
}