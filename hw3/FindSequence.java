package hw3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class FindSequence {

	public static void main(String[] args) {
		int matrix[][] = readMatrix();
		int number = 0;

		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == number) {
					
					System.out.println("s�f�r�n koordinat� " + i + "-" + j);
					
					System.out.println(Sequence(matrix,i,j,0));
						
				}
			}
		}
		
		printMatrix(matrix);

	}
	
	private static int Sequence (int matrix[][],int x,int y, int number) {
		
		System.out.println("x=" + x + " y=" + y + " aranan say�" + number);
		
		
		if (number == 4) {
			
			System.out.println("bulundu");
			return number ;
			
		}
		else {
			
			try {
				if ((matrix[--x][y] == ++number) ) {
					number++;
					System.out.println(number + "solunda");
					System.out.println(matrix[--x][y]);
					return Sequence(matrix, --x, y, number+1);
				}
				else if (matrix[++x][y] == ++number) {
					number++;
					System.out.println(number + "sa��nda");
					System.out.println(matrix[++x][y]);
					return Sequence(matrix, ++x, y, number+1);
				}
				else if (matrix[x][--y] == ++number) {
					number++;
					System.out.println(number + "alt�nda");
					System.out.println(matrix[x][--y]);
					return Sequence(matrix, x, --y, number+1);
				}
				else if (matrix[x][++y] == ++number) {
					number++;
					System.out.println(number + "�st�nde");
					System.out.println(matrix[x][++y]);
					return Sequence(matrix, x, ++y, number+1);
				}
			}
			
			catch (ArrayIndexOutOfBoundsException e) {
				e.getStackTrace();
			}
			
		}
		return 123132123 ;

		
	}



	private static int[][] readMatrix() {
		int[][] matrix = new int[10][10];
		File file = new File("C:\\Users\\ozank\\eclipse-workspace\\hw3\\src\\hw3\\matrix.txt");

		try {

			Scanner sc = new Scanner(file);
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
